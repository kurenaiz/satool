### Sakura's Archive Tool (satool)

satool wraps around a bunch of archive programs, so you can extract, list and pack files with a single interface,
check the EXAMPLES section in the manual.

satool is inspired by atool, but in lua instead of perl, the code is much simpler + some other features. Requires lua 5.2 or higher

```
$ man satool
```

Almost a drop-in replacement, check Differences for more info.

## Additional Features

- Much simpler and small codebase (around 50% lines of code)

- lua instead of perl

- More formats: we support more formats like zstd (like atool2), and some
obscures of our own like windows images (.wim)

## Differences

- Defaults: atool uses a different program for each format type, satool tries to use 7z
first for everything, falling back to other programs when not available.

- Format support: check the supported dictionary at the beginning of the satool file in this repo,
not every format atool supports is supported yet, a large potion is supported, but the most obscure
ones are not (yet), it's pretty easy to add support tho, so if you need something, just tell me.

- Fallbacks: As noted before, satool provides a fallback mechanism, it tries to use multiple
programs which support a given operation on a given format, before failing.

- No diff: no diff feature yet.

- Limited cat support: cat support is yet limited.

- Options: some options are not supported anymore.
    - -o, -O are redudant, satool favors editing the .lua file which can
    easily be done, instead of config options, it also has fallbacks, so there's
    no need to have a config option to use another program, like atool does.

    - config file: Not supported, same reason as above.

    - -e: the each flag is useful, but unnecessary, satool favors your shell's 'for' loop
    instead of a convenience flag.

    - -v: now shows the version, there's no verbosity control, you can either make each archive
    program quiet with -q(TODO), that way only satool messages are shown. Or you can redirect everything
    to /dev/null .e.g. (sunpack archive.tar.gz >/dev/null 2>&1)

## TODO

tests
