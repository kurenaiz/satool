.\"                                      -*- nroff -*-
.\" satool.1 - Manual page for satool.
.\"
.\" Copyright (C) 2023 Duarm
.\"
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU Library General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License along
.\" with this program; if not, write to the Free Software Foundation,
.\" Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
.\"
.TH SATOOL "1" "June 10, 2023" "satool"
.\" Read this file with groff -man -Tascii satool.1
.SH NAME
satool \- A script for managing file archives of various types
.SH SYNOPSIS
.B satool
.RI [ OPTION ]... " ARCHIVE " [ FILE ]...
.br
.B sunpack
.RI [ OPTION ]... " ARCHIVE " [ FILE ]...
.br
.B spack
.RI [ OPTION ]... " ARCHIVE " [ FILE ]...
.br
.B sls
.RI [ OPTION ]... " ARCHIVE " [ FILE ]...
.br
.B scat
.RI [ OPTION ]... " ARCHIVE " [ FILE ]...
.br
.B sdiff
.RI [ OPTION ]... " ARCHIVE " "" ARCHIVE
.br
.B srepack
.RI [ OPTION ]... " OLD-ARCHIVE " "" NEW-ARCHIVE
.SH DESCRIPTION
This manual page document describes the \fBsatool\fP commands.
These commands are used for managing file archives of various
types, such as tar and Zip archives. Each command can be
executed individually or by giving the appropriate options
to \fBsatool\fP (see \fBOPTIONS\fP below).
.PP
\fBsunpack\fP extracts files from an archive. Often one wants
to extract all files in an archive to a single subdirectory.
However, some archives contain multiple files in their root
directories. The sunpack program overcomes this problem by
first extracting files to a unique (temporary) directory, and
then moving its contents back if possible. This also prevents
local files from being overwritten by mistake.
.PP
\fBspack\fP creates archives (or compresses files). If no file
arguments are specified, filenames to add are read from standard in.
.PP
\fBsls\fP lists files in an archive.
.PP
\fBscat\fP extracts files in an archive to standard out.
.PP
\fBsdiff\fP generates a diff between two archives using
diff(1).
.PP
\fBsrepack\fP repacks archives to a different format. It does
this by first extracting all files of the old archive into a
temporary directory, then packing all files extracted to
that directory to the new archive. Use the \-\-each (\-e) option in
combination with \-\-format (\-F) to repack multiple archives using a
single invocation of satool. Note that srepack will not remove the old
archive.
.PP
Unless the \fB\-\-format\fP (\fB\-F\fP) option is provided,
the archive format is determined by the archive file extension. I.e.
an extension ".tar.gz" or ".tgz" means tar+gzip format. Note that
the extensions are checked in the order listed in the section
\fBARCHIVE TYPES\fP below, which is why a file with extension ".tar.gz"
is considered to a be tar+gzip archive, not a gzip compressed file.
.SH OPTIONS
These programs follow the usual GNU command line syntax, with long
options starting with two dashes (`-').
A summary of options is included below.
.TP
.B \-l, \-\-list
List files in archive.
This option is automatically assumed when \fBsls\fP is executed.
.TP
.B \-x, \-\-extract
Extract files from archive.
This option is automatically assumed when \fBsunpack\fP is executed.
.TP
.B \-X, \-\-extract-to\fR=\fIPATH\fR
Extract files from archive to the specified directory. When
unpacking compressed files, PATH may refer to either a filename
or an existing directory.
.TP
.B \-a, \-\-add
Create archive.
This option is automatically assumed when \fBspack\fP is executed.
.TP
.B \-c, \-\-cat
Extract a file from archive to standard out (displaying it on
screen).
This option is automatically assumed when \fBscat\fP is executed.
.TP
.B \-d, \-\-diff
Extract two archives and use diff(1) to generate differencies
between them.
This option is automatically assumed when \fBsdiff\fP is executed.
.TP
.B \-S, \-\-simulate
Run satool in simulation mode. No changes to the filesystem
(i.e. writes) will be made, and all commands that would be
executed are displayed instead. This option can't be combined
with \fB\-\-explain\fP (since it implies that already).

Note that it is not guaranteed that the commands printed in
simulation mode will be the same as those executed in non-
simulation mode. This is because some operations depend on
what files archives contain, and satool can at this time
only determine that by extracting archives.
.TP
.B \-E, \-\-explain
Display commands executed by satool. This option can't be combined
with \fB\-\-simulate\fP.
.TP
.B \-f, \-\-force
When extracting from files, allow overwriting of local files.
When creating an archive, allow the archive file to be overwritten
if it already exists. Note that it is possible to add files to
existing RAR and Zip archives (this is not possible for many
other formats).
.TP
.B \-D, \-\-subdir
When extracting archives, always create a new directory for
the archive even if the archive only contains one file in
its root directory.
.TP
.B \-0, \-\-null
If no file arguments are specified when creating or adding files
to archives, the list of files will be read from standard in.
Normally these filenames are separated by newline, but with this
option they are separated by null-bytes. This is useful with the
GNU find \-print0 option.
.TP

.B \-O, \-\-format\-option\fR=\fIOPTION\fR
Send additional options to the archiver command. This can be useful
when specifying compression options for some archives, e.g.
.br
        \fBspack \-F7z \-O-mx=9 archive.7z dir\fP
.br
You can specify this multiple times add different options.
.TP
.B \-\-save\-outdir\fR=\fIFILE\fR
When extracting files, save the name of the directory which
the archive was extracted to to the specified file. If the
command was not `extract', or the archive was not extracted to
a new directory, then nothing will be written to the specified
file. If multiple archives were specified (with \-e), then
only the last directory that files were extracted to will be
written to FILE.

This option is used internally (see \fBEXAMPLES\fR below).
.TP
.B \-\-help
Show summary of options.
.TP
.B \-\-version
Output version information and exit.
.SH ARCHIVE TYPES
Unless the \-f (\-\-format) option is provided, the archive format
is determined by the archive file extension. I.e. an extension
".tar.gz" or ".tgz" means tar+gzip format. Note that the extensions
are checked in the other listed above, which is why a file
with extension ".tar.gz" is considered to a tar+gzip archive,
not a gzip archive.
.PP
The supported archive types are:
.TP
.RI \fBtar+gzip\fP " " ( .tar.gz ", " .tgz )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+bzip\fP " " ( .tar.bz ", " .tbz )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+bzip2\fP " " ( .tar.bz2 ", " .tbz2 )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+compress\fP " " ( .tar.Z ", " .tZ )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+lzop\fP " " ( .tar.lzo ", " .tzo )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+lzip\fP " " ( .tar.lz ", " .tlz )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+xz\fP " " ( .tar.xz ", " .txz )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+7z\fP " " ( .tar.7z ", " .t7z )
Extract, Pack, List, Cat.
.TP
.RI \fBtar+zstd\fP " " ( .tar.zst ", " .tzst )
Extract, Pack, List, Cat.
.TP
.RI \fBtar\fP " " ( .tar )
Extract, Pack, List, Cat.
.TP
.RI \fBzip\fP " " ( .zip )
Extract, Pack, List, Cat.
.TP
.RI \fBwim\fP " " ( .wim )
Extract, Pack, List.
.TP
.RI \fBjar\fP " " ( .jar ", " .war )
Extract, Pack, List, Cat.
.TP
.RI \fBrar\fP " " ( .rar )
Extract, Pack, List, Cat.
.TP
.RI \fBlha\fP " " ( .lha ", " .lzh )
Extract, Pack, List, Cat.
.TP
.RI \fB7z\fP " " ( .7z )
Extract, Pack, List
.TP
.RI \fBalzip\fP " " ( .alz )
Extract 
.TP
.RI \fBace\fP " " ( .ace )
Extract, List 
.TP
.RI \fBar\fP " " ( .a )
Extract, Pack, List, Cat.
.TP
.RI \fBarj\fP " " ( .arj )
Extract, Pack, List.
.TP
.RI \fBarc\fP " " ( .arc )
Extract, Pack, List, Cat.
(Note that arc outputs an extra newline when the cat command is used.)
.TP
.RI \fBrpm\fP " " ( .rpm )
Extract, List 
.TP
.RI \fBdeb\fP " " ( .deb )
Extract, List 
.TP
.RI \fBcab\fP " " ( .cab )
Extract, List, Cat 
.TP
.RI \fBgzip\fP " " ( .gz )
Extract, Pack, Cat 
.TP
.RI \fBbzip\fP " " ( .bz )
Extract, Pack, Cat 
.TP
.RI \fBbzip2\fP " " ( .bz2 )
Extract, Pack, Cat 
.TP
.RI \fBcompress\fP " " ( .Z )
Extract, Pack, Cat 
.TP
.RI \fBlzma\fP " " ( .lzma )
Extract, Pack, Cat 
.TP
.RI \fBlzop\fP " " ( .lzo )
Extract, Pack  

The cat command is not supported
because lzop does not want to extract files to standard out unless the \-f
flag is given.
.TP
.RI \fBlzip\fP " " ( .lz )
Extract, Pack, Cat 
.TP
.RI \fBxz\fP " " ( .xz )
Extract, Pack, Cat 
.TP
.RI \fBrzip\fP " " ( .rz )
Extract, Pack 
.TP
.RI \fBlrzip\fP " " ( .lrz )
Extract, Pack 
.TP
.RI \fB7zip\fP " " ( .7z )
Extract, Pack, List, Cat.
.TP
.RI \fBzstd\fP " " ( .zst )
Extract, Pack, List, Cat.
.TP
.RI \fBcpio\fP " " ( .cpio )
List, Extract, Pack 

.SH EXAMPLES

To extract all files from archive `foobar.tar.gz' to a subdirectory
(or the current directory if it only contains one file):
.br
	\fBsunpack foobar.tar.gz\fP
.PP
To extract all files from all `.tar.gz' archives in the
current directory: (TODO)
.br
	\fBsunpack \-e *.tar.gz\fP
.PP
To create a zip archive of two files `foo' and `bar':
.br
	\fBspack myarchive.zip foo bar\fP
.PP
To display the file `baz' in the archive `myarchive.zip'
through a pager:
.br
	\fBscat \-p myarchive.zip baz\fP
.PP
To list contents of the rar archive `stuff.rar':
.br
	\fBsls stuff.rar\fP
.PP
To create three archives, `dir1.tar.gz', `dir2.tar.gz' and `dir3.tar.gz',
so that the first one contains all files in dir1, the second all
in dir2 and the third all dir3: (TODO)
.br
	\fBspack \-e \-F .tar.gz dir1 dir2 dir3\fP
.PP
To show all differences between version 2.4.17 and 2.4.18 of the kernel: (TODO)
.br
	\fBsdiff linux-2.4.17.tar.gz linux-2.4.18.tar.gz\fP
.PP
To repack all .tar.gz archives in the current directory to .tar.7z (the
old archive will be kept untouched): (TODO)
.br
	\fBsrepack \-F.tar.7z \-e *.tar.gz\fP
.PP
Here's a shell function that will make the sunpack command change into the
directory where files were extracted:
.br
	\fBsunpack () {\fP
.br
	\fB  TMP=`mktemp /tmp/sunpack.XXXXXXXXXX`\fP
.br
	\fB  satool \-x \-\-save-outdir=$TMP "$@"\fP
.br
	\fB  DIR="`cat $TMP`"\fP
.br
	\fB  [ "$DIR" != "" \-a \-d "$DIR" ] && cd "$DIR"\fP
.br
	\fB  rm $TMP\fP
.br
	\fB}\fP
.br
If you don't have the mktemp program, you can replace the second line with
(note however that this is not entirely safe)
.br
	\fB  TMP="/tmp/satool_outdir.$$"\fP
.PP
.SH KNOWN BUGS
-
.PP
If you find a bug not listed here, please report it to <duarm0@disroot.org>.
.SH REPORTING BUGS
Report bugs to https://gitlab.com/kurenaiz/satool/-/issues.
.SH AUTHOR
The author of \fBsatool\fP and this manual page is Duarm <\fdduarm0@disroot.org\fP>.

Inspired by atool by Oskar Liljeblad
.SH COPYRIGHT
Copyright \(co 2023 Duarm

This is free software; see the source for copying conditions.  

There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
